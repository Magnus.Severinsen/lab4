package cellular;

import datastructure.CellGrid;
import datastructure.IGrid;

import java.util.Random;

public class BriansBrain implements CellAutomaton {
    /**
     * The grid of cells
     */
    IGrid currentGeneration;

    /**
     *
     * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
     * provided size
     *
     * @param height The height of the grid of cells
     * @param width  The width of the grid of cells
     */
    public BriansBrain(int rows, int columns) {
        currentGeneration = new CellGrid(rows, columns, CellState.DEAD);
        initializeCells();
    }

    @Override
    public void initializeCells() {
        Random random = new Random();
        for (int row = 0; row < currentGeneration.numRows(); row++) {
            for (int col = 0; col < currentGeneration.numColumns(); col++) {
                if (random.nextBoolean()) {
                    currentGeneration.set(row, col, CellState.ALIVE);
                } else {
                    currentGeneration.set(row, col, CellState.DEAD);
                }
            }
        }
    }

    @Override
    public int numberOfRows() {
        return currentGeneration.numRows();
    }

    @Override
    public int numberOfColumns() {
        return currentGeneration.numColumns();
    }

    @Override
    public CellState getCellState(int row, int col) {
        return currentGeneration.get(row,col);
    }

    @Override
    public void step() {
        IGrid nextGeneration = currentGeneration.copy();
        for (int x = 0; x < numberOfRows(); x++){
            for (int y = 0;  y < numberOfColumns(); y++){
                nextGeneration.set(x,y,getNextCell(x,y));
            }
        }
        currentGeneration = nextGeneration;
        // TODO
    }

    @Override
    public CellState getNextCell(int row, int col) {
        // TODO
        CellState state = getCellState(row, col);
        int neighborCount = countNeighbors(row,col,CellState.ALIVE);
        if (state.equals(CellState.ALIVE)) {
            return CellState.DYING;
        }
        else if (state.equals(CellState.DYING)) {
            return CellState.DEAD;
        }
        else {
            if (neighborCount == 2) {
                return CellState.ALIVE;
            }

        }
        return CellState.DEAD;
    }

    /**
     * Calculates the number of neighbors having a given CellState of a cell on
     * position (row, col) on the board
     *
     * Note that a cell has 8 neighbors in total, of which any number between 0 and
     * 8 can be the given CellState. The exception are cells along the boarders of
     * the board: these cells have anywhere between 3 neighbors (in the case of a
     * corner-cell) and 5 neighbors in total.
     *
     * @param x     the x-position of the cell
     * @param y     the y-position of the cell
     * @param state the Cellstate we want to count occurences of.
     * @return the number of neighbors with given state
     */
    private int countNeighbors(int row, int col, CellState state) {
        // TODO
        int numOfNeighbors = 0;
        for (int x = row-1; x <= row+1;x++){
            if (x < 0 || x >= numberOfRows()){continue;}
            for (int y = col-1; y <= col+1; y++){
                if (y < 0 || y >= numberOfColumns()){continue;}
                if (x == row && y == col){continue;}
                CellState current = getCellState(x,y);
                if (current == state) {
                    numOfNeighbors++;
                }
            }
        }
        return numOfNeighbors;
    }

    @Override
    public IGrid getGrid() {
        return currentGeneration;
    }
}
