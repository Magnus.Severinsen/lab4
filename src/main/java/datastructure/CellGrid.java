package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {

    private int rows;
    private int cols;
    CellState initialState;
    CellState [][] grid;


    public CellGrid(int rows, int columns, CellState initialState) {
		// TODO Auto-generated constructor stub
        this.rows = rows;
        this.cols = columns;
        this.initialState = initialState;
        grid = new CellState[rows][columns];
        for (int i = 0; i<rows; i++){
            for (int j = 0; j<columns; j++){
                grid[i][j] = initialState;
            }
        }

	}

    @Override
    public int numRows() {
        // TODO Auto-generated method stub
        return rows;
    }

    @Override
    public int numColumns() {
        // TODO Auto-generated method stub
        return cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        // TODO Auto-generated method stub
        if (row < 0 || row >= numRows()){
            throw new IndexOutOfBoundsException();
        }
        if (column < 0 || column >= numColumns()) {
            throw new IndexOutOfBoundsException();
        }
        grid[row][column] = element;

        
    }

    @Override
    public CellState get(int row, int column) {
        // TODO Auto-generated method stub
        if (row < 0 || row >= numRows()){
            throw new IndexOutOfBoundsException();
        }
        if (column < 0 || column >= numColumns()) {
            throw new IndexOutOfBoundsException();
        }
        return grid[row][column];
    }

    @Override
    public IGrid copy() {
        // TODO Auto-generated method stub
        CellGrid copyCell = new CellGrid(numRows(),numColumns(), initialState);
        for (int i = 0; i<rows; i++){
            for (int j = 0; j<cols; j++) {
                copyCell.set(i, j, get(i, j));
            }
        }
        return copyCell;
    }
    
}
